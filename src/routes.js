import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './containers/App';
import Wrapper from './components/Wrapper';

export default (
	<Route path='/' component={Wrapper} >
		<IndexRoute component={App} />
		<Route path='city/:id' component={App} />
	</Route>
);
