import fetch from 'isomorphic-fetch'

export const REQUEST_COUNTRIES = 'REQUEST_COUNTRIES'
export const RECEIVE_COUNTRIES = 'RECEIVE_COUNTRIES'


export function fetchCountries() {
  return dispatch => {
    dispatch(requestCountries())
    return fetch('https://restcountries.eu/rest/v1/all')
      .then(response => response.json())
      .then(json => dispatch(receiveCountries(json)))
  }
}
function requestCountries() {
  return {
    type: REQUEST_COUNTRIES
  }
}

function receiveCountries(json) {
  return {
    type: RECEIVE_COUNTRIES,
    //countries: json.map(child => child.data),
    countries: json
  }
}