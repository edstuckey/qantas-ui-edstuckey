import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchCountries } from '../actions'
import Countries from '../components/Countries'
import City from '../components/City'
import Loader from '../components/Loader'
import SearchHeader from '../components/SearchHeader'


class App extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.fetchCountries();
  }

  getCountryData(countryCode) {
    let countryIndex = 0;
    for (let [index, value] of this.props.countries.entries()) {
      if(value.alpha3Code.toLowerCase() == this.props.params.id){
        countryIndex = index;
        break;
      }
    }
    return this.props.countries[countryIndex];
  }
  
  searchCity(term) {
    let searchIndex = -1;
    if(term.length >= 3){
      for (let [index, value] of this.props.countries.entries()) {
        if(value.capital.toLowerCase() == term.toLowerCase()) {
          //searchIndex = index;
          browserHistory.push(`/city/${this.props.countries[index].alpha3Code.toLowerCase()}`)
          break;
        }
      }
    }else if(term.length == 0){
      browserHistory.push('/');
    }
  }

  render() {
    const { countries, isFetching } = this.props
    const isEmpty = countries.length === 0
    const loadCity = this.props.params.id != undefined

    return (
      <div className="main">
        {isEmpty ? (isFetching ? <Loader /> : <h2>Error, no countries returned.</h2>)
          : <div>
              <SearchHeader onSearchTermChanged={term => this.searchCity(term)} onSearchClicked={term => this.searchCity(term)} />
              {loadCity ? <City countryData={this.getCountryData(this.props.params.id)} /> : <Countries countries={countries} /> }   
            </div>
        }
      </div>
    )
  }
}

App.propTypes = {
  countries: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({ fetchCountries }, dispatch);
}

function mapStateToProps(state) {
  return{
    isFetching: state.countriesData.isFetching,
    countries: state.countriesData.items
  }  
}

export default connect(mapStateToProps, mapDispatchToProps)(App)