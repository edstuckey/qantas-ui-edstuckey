import React, { PropTypes, Component } from 'react'
import { browserHistory } from 'react-router'
import { List, ListItem, IconButton } from 'react-mdl';

export default class City extends Component {
  
  constructor(props) {
    super(props)
    this.onBackClicked = this.onBackClicked.bind(this);
  }

  onBackClicked(){
    browserHistory.push('/');
  }

  render() {
    let data = this.props.countryData
    
    return (
      <div >
        <IconButton name="keyboard_backspace" colored onClick={this.onBackClicked} />
        <p><strong>Capital City: </strong>{data.capital}</p>
        <p><strong>Country:</strong> {data.name}</p>
        <p><strong>Region:</strong> {data.region}</p>
        <p><strong>Population:</strong> {data.population.toLocaleString()}</p>
        <p><strong>Lat/Lng:</strong> {data.latlng[0]}/{data.latlng[1]}</p>
        <p><strong>Timezone:</strong> {data.timezones}</p>
        <p><strong>Calling Code:</strong> {data.callingCodes}</p>
      </div>
    )
  }
}

City.propTypes = {
  countryData: PropTypes.object.isRequired
}
