import React, { Component } from 'react';
import { Spinner } from 'react-mdl';

export default class Loader extends Component {
	
	render() {
		return (
		  <div className="loading-wrapper">
		  	<Spinner />
		  </div>
		);
	}
}