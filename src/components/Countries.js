import React, { PropTypes, Component } from 'react'
import { Link } from 'react-router';
import { List, ListItem } from 'react-mdl';

export default class Countries extends Component {

  shuffle(array) {
    let counter = array.length;
    while (counter > 0) {
        let index = Math.floor(Math.random() * counter);
        counter--;
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }
    return array.slice(0,5);
  }

  render() {
    const randomCountries = this.shuffle(this.props.countries);
    return (
      <List>
        {randomCountries.map((country, i) =>
            <Link key={country.alpha3Code} to={`/city/${country.alpha3Code.toLowerCase()}`}>
              <ListItem key={i}>{country.capital}</ListItem>
            </Link>
        )}
        
      </List>

    )
  }
}

Countries.propTypes = {
  countries: PropTypes.array.isRequired
}
