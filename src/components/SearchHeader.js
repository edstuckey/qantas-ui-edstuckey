import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { searchForCountry } from '../actions'
import { Textfield } from 'react-mdl';

class SearchHeader extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchTerm: ''
    };
    this.onInputChange = this.onInputChange.bind(this);
  }
  
  onInputChange(term){
    this.setState({
      searchTerm: term
    });
    this.props.onSearchTermChanged(term);
  }

  render() {
    return (
      <div className="formWrapper">
        <form onSubmit={event => event.preventDefault()}>
          <div className="formSearch" >
          <Textfield onChange={event => this.onInputChange(event.target.value)} label="Search for city..." />
          </div>
        </form>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({ searchForCountry }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchHeader)