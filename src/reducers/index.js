import { combineReducers } from 'redux'
import { REQUEST_COUNTRIES, RECEIVE_COUNTRIES } from '../actions'

function countriesData(state = {
  isFetching: false,
  items: []
}, action) {
  switch (action.type) {
    case REQUEST_COUNTRIES:
      return Object.assign({}, state, {
        isFetching: true
      })
    case RECEIVE_COUNTRIES:
      return Object.assign({}, state, {
        isFetching: false,
        items: action.countries
      })
    default:
      return state
  }
}

const rootReducer = combineReducers({
  countriesData
})

export default rootReducer
